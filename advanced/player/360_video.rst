############################
360° Video playback with VLC
############################

.. warning:: Not to be confused with 360° and 3D video. A 360° Video emulates a free point of view and a 3D video projects depth of perception.  

`360°(degree) display <https://en.wikipedia.org/wiki/360-degree_video>`_ is a process of emulating an omnidirectional video shot while at the same time creating an immersive effect on the viewer. They are shot using either a single camera with multiple lenses or a platform with multiple cameras. It is processed to 'stitch' the footage in multiple angles and render them into one single media file with a 'free view' to experience `virtual reality <https://en.wikipedia.org/wiki/Virtual_reality>`_ . 


These are different 360° video displays based on projection methods:

* `Equirectangular - <https://wiki.panotools.org/Equirectangular_Projection>`_ A type of projection mapping different portions of an image originally viewed in 360° into 
* `Fisheye - <https://wiki.panotools.org/Fisheye_Projection>`_ A type of projection which emulates strong distortion with extremely wide angles of view. It is termed after the belief that a fish views the underwater world in a similar fashion.   
* `Cubic Mapping - <https://en.wikipedia.org/wiki/Cube_mapping>`_ A type of projection where different portions of an image is stitched onto the layout of a cube. 


Out of all these, VLC supports 360° videos that use **Equirectangular type** of Projection.

.. note: Some 360° videos may be missing required metadata for playback. Convert the video into mp4 format and Refer `this <https://support.google.com/youtube/answer/6178631?hl=en>`_ link for troubleshooting.
   
******************************
How to playback the 360° video
******************************
1. Open VLC Media Player and go to :menuselection:`Media --> Open File` and browse to the directory of the video file and double click to open it (or) Press Ctrl+O and choose the video file to open it.

2. After opening the file, you can experience the 360° video by using any one these methods.     

========
Method 1
========

This method uses the Mouse for navigation in the video.
Hold the left-mouse button and move vertically to change vertical viewing angle and horizontally to change horizontal viewing angle. 
The scroll button can be used for Zooming In/Out.  

.. figure:: /images/advanced/player/360_video_mouse_walkthrough.jpg 
   :align: center
   
   
========
Method 2
========

This method uses the keyboard keys for navigation in the video.

+---------------------+---------------------+
|   Action            | Keyboard Key(s)     |                         
+=====================+=====================+
|   Zoom In/Out       | Page Up/Page Down   |
+---------------------+---------------------+
|Vertical View Angle  |   Up/Down Arrow     |
+---------------------+---------------------+
|Horizontal View Angle| Left/Right Arrow    |
+---------------------+---------------------+
